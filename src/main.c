#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#include <assert.h>
#include <stdio.h>

void debug(const char *fmt, ...);

struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void test_malloc() {
    debug("Test 1: Heap initialization");
    void *heap = heap_init(REGION_MIN_SIZE);
    assert(heap);

    struct block_header *header = block_get_header(_malloc(100));

    assert(header->capacity.bytes == 100);
    assert(header->is_free == false);

    heap_term();
    debug("\nTest 1 is passed\n");
}

void test_single_free() {
    debug("Test 2: Running single free");
    void *heap = heap_init(REGION_MIN_SIZE);
    assert(heap);
    debug("Heap was initialized with %d bytes\n", 0);



    void *pointer1 = _malloc(100);
    void *pointer2 = _malloc(100);
    void *pointer3 = _malloc(100);

    struct block_header *block1 = block_get_header(pointer1);
    struct block_header *block2 = block_get_header(pointer2);
    struct block_header *block3 = block_get_header(pointer3);

    assert(!block1->is_free);
    assert(!block2->is_free);
    assert(!block3->is_free);

    _free(block2);

    assert(!block1->is_free);
    assert(block2->is_free);
    assert(!block3->is_free);

    heap_term();
    debug("\nTest 2 is passed\n");
}

void test_multiple_free() {
    debug("Test 3: Running multiple free");
    void *heap = heap_init(REGION_MIN_SIZE);
    assert(heap);

    void *pointer1 = _malloc(100);
    void *pointer2 = _malloc(100);
    void *pointer3 = _malloc(100);

    struct block_header *block1 = block_get_header(pointer1);
    struct block_header *block2 = block_get_header(pointer2);
    struct block_header *block3 = block_get_header(pointer3);

    assert(!block1->is_free);
    assert(!block2->is_free);
    assert(!block3->is_free);

    _free(block3);

    assert(!block1->is_free);
    assert(!block2->is_free);
    assert(block3->is_free);

    _free(block2);

    assert(!block1->is_free);
    assert(block2->is_free);
    assert(block2->capacity.bytes == offsetof(struct block_header, contents) + 100 * 2);

    heap_term();
    debug("\nTest 3 is passed\n");
}

void test_grow_heap() {
    debug("Test 4: Grow heap");
    void *heap = heap_init(REGION_MIN_SIZE);
    assert(heap);

    void *pointer = _malloc(REGION_MIN_SIZE);

    struct block_header *header = block_get_header(pointer);
    assert(header->capacity.bytes == REGION_MIN_SIZE);
    assert(size_from_capacity(header -> capacity).bytes > REGION_MIN_SIZE);

    heap_term();
    debug("\nTest 4 is passed\n");
}

int main() {
    debug("Running tests...\n");
    test_malloc();
    test_single_free();
    test_multiple_free();
    test_grow_heap();
    debug("Tests is passed.\n");
    return 0;
}

